export interface Project {
    name: string;
    viewValue: string;
    value: string;
}
