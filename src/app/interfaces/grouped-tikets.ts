import { Ticket } from './ticket';
export interface GroupedTikets {
    project: string;
    tickets: Array<Ticket>;
    allHours: number;
    dataSource: Array<any>;
  }