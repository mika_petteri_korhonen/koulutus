export interface Customer {
    id: number;
    comppany: string;
    contact: Array<Contact>;
    address: Address;
    vatId: string;
}
export interface Contact {
    name: string;
    phone: string;
    address: string;
    email: string;
}
export interface Address {
    address: string;
    postalCode: string;
    country: string;
    city: string;
}