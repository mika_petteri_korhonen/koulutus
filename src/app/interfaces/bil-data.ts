export interface BilData {
    name: string;
    fullPrice: number;
    hours: number;
    advance: number;
    service: number;
    lightning: number;
    accidentInsurance: number;
    socialsecurity: number;
    tax: number;
    yel: number;
    vatReduction: number;
}
