export interface NgxGraphData {
    name: string;
    value: number;
}
export interface NgxGrapDataObject {
    name: string;
    data: NgxGrapDataDataObject;
    incomeStatement: NgxGrapDataIncomeStatement
}
export interface NgxGrapDataDataObject {
    lasku: Array<NgxGraphData>;
    palkka: Array<NgxGraphData>;
}
export interface NgxGrapDataIncomeStatement {
    "300": number;
    "301": number;
    "312": number;
    "313": number;
    "314": number;
    "315": number;
    "317": number;
    "318": number;
    "319": number;
    "323": number;
    "324": number;
    "333": number;
    "334": number;
    "335": number;
    "336": number;
    "337": number;
    "341": number;
    "343": number;
    "344": number;
    "346": number;
    "349": number;
    "353": number;
    "358": number;
    "359": number;
    "365": number;
    "366": number;
    "367": number;
}