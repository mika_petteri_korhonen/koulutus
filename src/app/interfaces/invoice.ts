import { Ticket } from '../interfaces/ticket';
export interface Invoice {
    name: string;
    dates: InvoiceDates;
    base64: string;
    fullPrice: number;
    price: number;
    paid: number;
    due: number;
    vat: number;
    overBilled: number;
    tickets: Array<Ticket>;
}
export interface InvoiceDates {
    due: Date;
    sent: Date;
    dueFinish: string;
}