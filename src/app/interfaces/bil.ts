export interface Bil {
    name: string;
    startDate: Date;
    endDate: Date;
    sent: boolean;
    paid: boolean;
    advance: boolean;
    lightning: boolean;
    customerId: number;
    
}
