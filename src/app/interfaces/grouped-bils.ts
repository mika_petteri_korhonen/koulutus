import { Ticket } from './ticket';
import { GroupedTikets } from './grouped-tikets';
export interface GroupedBils {
    name: string;
    startDate: Date;
    endDate: Date;
    sent: boolean;
    paid: boolean;
    advance: boolean;
    lightning: boolean;
    billed: number;
    customerId: number;
    tickets: Array<Ticket>;
    groupedTickets: Array<GroupedTikets>;
    bilProjectArr: Array<DataSource>;
    dataSource: Array<ProjectDataSource>;
    allHours: number;
}
export interface DataSource {
    name: string;
    value: number;
}
export interface ProjectDataSource {
    project: string;
    hours: number;
}
