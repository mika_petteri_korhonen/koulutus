export interface User {
    name: string;
    address: string;
    postalNumber: string;
    city: string;
    country: string;
    bankConnections: Array<BankAccounts>;
    invoiceReferences: Array<References>,
    vatId: string;
    phoneNumber: string;
    userName: string;
}
export interface References {
    reference: string;
}
export interface BankAccounts {
    name: string;
    iban: string;
    swift: string;
}