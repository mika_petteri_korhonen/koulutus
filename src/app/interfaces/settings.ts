export interface Settings {
  service: number;
  advance: number;
  lightning: number;
  accidentInsurance: number;
  socialsecurity: number;
  tax: number;
  hourRate: number;
  vat: number;
  socialCecurityVat: number;
  yel: number;
}
