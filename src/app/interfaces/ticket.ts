import { CardData } from './card-data';
export interface Ticket {
    cardData: CardData;
    project: string;
    billing: boolean;
    date: string;
    startTime: Date;
    endTime: Date;
    hours: string;
    startTimeHuman: string;
    endTimeHuman: string;
}
export interface TicketOld {
    message: string;
    project: string;
    billing: boolean;
    date: string;
    startTime: Date;
    endTime: Date;
    hours: string;
    startTimeHuman: string;
    endTimeHuman: string;
}