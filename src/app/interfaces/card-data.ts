export interface CardData {
    ticketMessage: string;
    timeSpent: string;
    billable: boolean;
    project: string;
    name: string;
    header: string;
    sub: string;
    minutes: number
    class: string
    image: string;
}
