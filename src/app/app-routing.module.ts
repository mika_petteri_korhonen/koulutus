import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CombinedPageComponent } from './components/combined-page/combined-page.component';
import { AktiviteetitComponent } from './components/aktiviteetit/aktiviteetit.component';
import { MajoituksetComponent } from './components/majoitukset/majoitukset.component';

const routes: Routes = [
  { path: '', component: CombinedPageComponent },
  { path: 'Majoitukset/:kohdeCode/:alkupvm/:loppupvm/:henkilot', component: MajoituksetComponent },
  { path: 'Aktiviteetit/:kohdeCode/:alkupvm/:loppupvm/:henkilot', component: AktiviteetitComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
