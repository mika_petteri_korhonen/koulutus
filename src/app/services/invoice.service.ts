import { Injectable } from '@angular/core';
import { Invoice, InvoiceDates } from '../interfaces/invoice';
import { TicketsService } from './tickets.service';
import { GroupedBils } from '../interfaces/grouped-bils';
@Injectable({
  providedIn: 'root'
})
export class InvoiceService {
  public invoices: Array<Invoice> = [];
  public groupdeBils: Array<GroupedBils> = [];
  constructor(private ticketService: TicketsService) {
    this.updateInvoices();
  }
  updateInvoices() {
    let sentDate = new Date();
    let newDates: InvoiceDates = {
      due: new Date(sentDate.getTime() + 12096e5),
      sent: new Date(),
      dueFinish: this.getFinishDate(new Date(sentDate.getTime() + 12096e5))
    };
    let paidAmaunt = 0;
    let newInvoice: Invoice = {
      name: 'Verkkokauppa',
      dates: newDates,
      base64: "",
      fullPrice: 0,
      price: 0,
      paid: paidAmaunt,
      due: 0,
      vat: 0,
      overBilled: 0,
      tickets: []
    }
    if(!this.checkIfInvoiceExists(newInvoice)) {
      this.invoices.push(newInvoice);
    }
  }
  getFinishDate(date) {
    let refDate = date;
    var day = refDate.getDate(); // Get the day 
    var month = refDate.getMonth() + 1; // The month start to zero
    var year = refDate.getFullYear();// Get the year 
    return day + '.' + month + '.' + year;
  }
  checkIfInvoiceExists(invoice) {
    return this.invoices.some((elem, ind) => {
      return (JSON.stringify(elem) === JSON.stringify(invoice)) ? true : false;
    });
  }
}
