import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable, BehaviorSubject } from 'rxjs'
import * as backend from './backend';

@Injectable({
  providedIn: 'root'
})
export class ProductKohdeService {
  public kohde: BehaviorSubject<any> = new BehaviorSubject(null);
  public kohdeObservable: Observable<any> = this.kohde.asObservable();
  public backendUrl = backend.conf.url;
  
  constructor(private http: Http) {
    this.getKohdeElements();
  }
  getKohdeElements() {
    this.http.get(this.backendUrl + '/kohde').subscribe((response) => {
      let body = JSON.parse(response['_body']);
      this.kohde.next(body.kohde);
    });
  }
}
