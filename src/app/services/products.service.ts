import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable, BehaviorSubject } from 'rxjs'
import * as backend from './backend';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  public products: BehaviorSubject<any> = new BehaviorSubject(null);
  public productsObservable: Observable<any> = this.products.asObservable();
  public backendUrl = backend.conf.url;

  constructor(private http: Http) {
    this.getProductsElements();
  }
  getProductsElements() {
    this.http.get(this.backendUrl + '/products').subscribe((response) => {
      let body = JSON.parse(response['_body']);
      this.products.next(body.products);
    });
  }
}
