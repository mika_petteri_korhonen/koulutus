import { TestBed } from '@angular/core/testing';

import { ToolbarMenuService } from './toolbar-menu.service';

describe('ToolbarMenuService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ToolbarMenuService = TestBed.get(ToolbarMenuService);
    expect(service).toBeTruthy();
  });
});
