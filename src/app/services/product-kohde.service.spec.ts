import { TestBed } from '@angular/core/testing';

import { ProductKohdeService } from './product-kohde.service';

describe('ProductKohdeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProductKohdeService = TestBed.get(ProductKohdeService);
    expect(service).toBeTruthy();
  });
});
