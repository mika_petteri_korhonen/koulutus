import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable, BehaviorSubject } from 'rxjs'
import * as backend from './backend';

@Injectable({
  providedIn: 'root'
})
export class AccordionsService {
  public accordions: BehaviorSubject<any> = new BehaviorSubject(null);
  public accordionsObservable: Observable<any> = this.accordions.asObservable();
  public backendUrl = backend.conf.url;

  constructor(private http: Http) {
    this.getAccordionsElements();
  }
  getAccordionsElements() {
    this.http.get(this.backendUrl + '/accordions').subscribe((response) => {
      let body = JSON.parse(response['_body']);
      this.accordions.next(body.accordions);
    });
  }
}
