import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable, BehaviorSubject } from 'rxjs'
import * as backend from './backend';

@Injectable({
  providedIn: 'root'
})
export class CardsService {
  public cards: BehaviorSubject<any> = new BehaviorSubject(null);
  public cardsObservable: Observable<any> = this.cards.asObservable();
  public backendUrl = backend.conf.url;

  constructor(private http: Http) {
    this.getCardsElements();
  }
  getCardsElements() {
    this.http.get(this.backendUrl + '/cards').subscribe((response) => {
      let body = JSON.parse(response['_body']);
      this.cards.next(body.cards);
    });
  }
}
