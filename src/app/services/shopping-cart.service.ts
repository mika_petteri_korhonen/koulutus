import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs'
import * as backend from './backend';

@Injectable({
  providedIn: 'root'
})
export class ShoppingCartService {
  public shoppingCart: BehaviorSubject<any> = new BehaviorSubject(null);
  public shoppingCartObservable: Observable<any> = this.shoppingCart.asObservable();
  public backendUrl = backend.conf.url;

  constructor(private http: Http) {
    this.getShoppingCartElements();
  }
  getShoppingCartElements() {
    this.http.get(this.backendUrl + '/shoppingCart').subscribe((response) => {
      let body = JSON.parse(response['_body']);
      this.shoppingCart.next(body.shoppingCart);
    });
  }
  addProductToCart(product) {
    const httpOptions = {
      headers: new Headers({
        'Content-Type':  'application/json',
        'Authorization': 'my-auth-token'
      })
    };
    this.http.post(this.backendUrl + '/shoppingCart', product, httpOptions).subscribe((response) => {
      let body = JSON.parse(response['_body']);
      console.log(body);
      this.getShoppingCartElements();
    });
  }
}
