import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable, BehaviorSubject } from 'rxjs'
import { Project } from '../interfaces/project';
import { Bil } from '../interfaces/bil';
import { Ticket } from '../interfaces/ticket';
import { BilData } from '../interfaces/bil-data';
import { Settings } from '../interfaces/settings';
import { GroupedTikets } from '../interfaces/grouped-tikets';
import { GroupedBils, ProjectDataSource, DataSource } from '../interfaces/grouped-bils';
import { UserService } from './user.service';
import { User } from '../interfaces/user';
import * as CryptoJS from 'crypto-js';

@Injectable({
  providedIn: 'root'
})
export class TicketsService {

  public bils: Array<Bil> = [];

  public bilsBackend: BehaviorSubject<any> = new BehaviorSubject(null);
  public bilsBackendObservable: Observable<any> = this.bilsBackend.asObservable();
  
  public groupdeBilsArray: Array<any> = [];
  public ticketsArr: Array<Ticket> = [];

  public tickets: BehaviorSubject<any> = new BehaviorSubject(null);
  public ticketsObservable: Observable<any> = this.tickets.asObservable();

  public automaticTickets: BehaviorSubject<any> = new BehaviorSubject(null);
  public automaticTicketsObservable: Observable<any> = this.automaticTickets.asObservable();

  public groupdeBils: BehaviorSubject<any> = new BehaviorSubject(null);
  public groupdeBilsObservable: Observable<any> = this.groupdeBils.asObservable();

  public bilData: BehaviorSubject<any> = new BehaviorSubject(null);
  public bilDataObservable: Observable<any> = this.bilData.asObservable();
  
  public reportData: BehaviorSubject<any> = new BehaviorSubject(null);
  public reportDataObservable: Observable<any> = this.reportData.asObservable();

  public reportDataBackend: BehaviorSubject<any> = new BehaviorSubject(null);
  public reportDataBackendObservable: Observable<any> = this.reportDataBackend.asObservable();

  public allDataSourceArr: BehaviorSubject<any> = new BehaviorSubject(null);
  public allDataSourceArrObservable: Observable<any> = this.allDataSourceArr.asObservable();

  //public backendUrl = "http://192.168.87.168:8080";
  public backendUrl = "http://192.168.10.61:8080";
  
  public groupedTicketsUrl = this.backendUrl + "/tickets/grouped";
  public settingsUkko: Settings = {
    service: 0.05,
    advance: 0.0403,
    lightning: 12.10,
    accidentInsurance: 0.027,
    socialsecurity: 0.0107,
    tax: 0.02,
    hourRate: 25,
    vat: 0.24,
    socialCecurityVat: 0.2404,
    yel: 0
  };
  public settingsFreeLaskutus: Settings = {
    service: 0.04,
    advance: 0.03,
    lightning: 0,
    accidentInsurance: 0.02,
    socialsecurity: 0.0107,
    tax: 0.02,
    hourRate: 25,
    vat: 0.24,
    socialCecurityVat: 0.2397,
    yel: 0
  };
  public settings: Settings = this.settingsUkko;
  public projects = [];
  public ticketsLocal = [];
  public groupdeBilsLocal: Array<GroupedBils> = [];
  public dataSourceArr: Array<any> = [];
  public user: User;
  constructor(
    private http: Http,
    private userService: UserService) {
      this.user = this.userService.user;
      this.ticketsObservable.subscribe((response) => {
        if(response != null) {
          this.ticketsLocal = response;
        }
      });
      /*
      Moved to backend fetch
      this.getFromLocalStorage().then((response) => {
        this.tickets.next(this.ticketsArr);
        let tempBils: Bil[] = this.localStorage.getObject('bils');
        let tempProject: Project[] = this.localStorage.getObject('projects');
        this.projects = tempProject;
        this.bils = tempBils;
        this.groupTicketsLocal();
        this.allData();
      });*/
      //this.getBilsBackend();
      //this.getAutomaticTicketsBackend();
  }
  getAutomaticTicketsBackend() {
    this.http.get(this.backendUrl + '/automaticTickets/' + this.user.userName).subscribe((response) => {
      let body = JSON.parse(response['_body']);
      this.automaticTickets.next(body.tickets);
    });
  }
  setAutomaticTicketsBackend(tickets) {
    this.http.post(this.backendUrl + '/automaticTickets/' + this.user.userName, {
      tickets: tickets
    }).subscribe((response) => {
      console.log(response);
    });
  }
  getBilsBackend() {
    this.http.get(this.backendUrl + '/bils/' + this.user.userName).subscribe((response) => {
      let body = JSON.parse(response['_body']);
      this.bils = body.bils;
      this.bilsBackend.next(this.bils);
      this.getTicketsBackend();
    });
  }
  getTicketsBackend() {
    this.http.get(this.backendUrl + '/tickets/' + this.user.userName).subscribe((response) => {
      let body = JSON.parse(response['_body']);
      this.tickets.next(body.tickets);
      this.groupTicketsLocal();
      this.allData();
    });
  }
  sendHours(tickets) {
    this.ticketsArr.push(tickets);
    this.tickets.next(this.ticketsArr);
    this.groupTicketsLocal();
    this.allData();
  }
  getGroupdeTickets() {
    /*
    this.http.get(this.groupedTicketsUrl)
    .subscribe((data) => this.reportDataBackend.next(data));
    */
  }
  allData() {
    let tempArr: Array<any> = [];
    this.ticketsLocal.forEach((elem, ind) => {
      if(tempArr.indexOf(elem.project) === -1) {
        tempArr.push(elem.project);
      }
    });
    var dataSource = [];
    tempArr.forEach((elem, ind) => {
      var tempObj = {
        name: elem,
        value: 0
      };
      this.ticketsLocal.forEach((elem2, ind2) => {
        if(elem === elem2.project && elem2.billing) {
          var endDate = new Date(elem2.endTime);
          var startTime = new Date(elem2.startTime);
          var tunnit = endDate.getTime() - startTime.getTime();
          tempObj.value += (tunnit / (1000 * 60 * 60) % 24);
        }
      });
      if(tempObj.value) {
        dataSource.push(tempObj);
      }
    });
    dataSource.sort((a, b) => {
      return b.value - a.value;
    });
    this.allDataSourceArr.next(dataSource);
  }
  groupTicketsLocal() {
    let tempArr = [];
    this.bils.forEach((elem, ind) => {
      let tempObj = JSON.parse(JSON.stringify(elem));
      tempObj.customerId = elem.customerId;
      tempObj.tickets = [];
      let startTime = new Date(tempObj.startDate).getTime();
      let endTime = new Date(tempObj.endDate).getTime();
      this.ticketsLocal.forEach((elem, ind) => {
        let startDate = new Date(elem.startTime).getTime();
        let startDateCheck = startDate - startTime;
        let endDateCheck = startDate - endTime;
        if(startDateCheck > 0 && endDateCheck < 0) {
          tempObj.tickets.push(elem);
        }
        if(tempArr.indexOf(elem.project) === -1) {
          tempArr.push(elem.project);
        }
      });
      this.groupdeBilsLocal.push(tempObj);
    });
    this.groupdeBilsLocal.forEach((elem, ind) => {
      elem.groupedTickets = [];
      elem.bilProjectArr = [];
      elem.dataSource = [];
      elem.allHours = 0;
      let allHours = 0;
      tempArr.forEach((elem2, ind2) => {
        var tempObj: GroupedTikets = {
          project: '',
          tickets: [],
          allHours: 0,
          dataSource: []
        };
        tempObj.project = elem2;
        elem.tickets.forEach((elem3, ind3) => {
          if(elem2 === elem3.project) {
            if(elem3.billing) {
              tempObj.tickets.push(elem3);
              var endDate = new Date(elem3.endTime);
              var startTime = new Date(elem3.startTime);
              var tunnit = endDate.getTime() - startTime.getTime();
              tempObj.allHours += (tunnit / (1000 * 60 * 60) % 24);
              allHours += (tunnit / (1000 * 60 * 60) % 24);
            }
          }
        });
        if(tempObj.allHours > 0) {
          elem.allHours += tempObj.allHours;
          let dataSource: ProjectDataSource = {
            project: elem2,
            hours: tempObj.allHours
          };
          elem.dataSource.push(dataSource);
        }
        elem.groupedTickets.push(JSON.parse(JSON.stringify(tempObj)));
        tempObj.tickets = [];
      });
      elem.groupedTickets.forEach((elem2, ind2) => {
        if(elem2.allHours > 0) {
          elem.bilProjectArr.push({
            name: elem2.project,
            value: elem2.allHours
          });
        }
      });
      let vat = (1+this.settings.vat);
      let price = elem.billed * this.settings.hourRate;
      let advance = (elem.advance) ? (price * this.settings.advance) * vat : 0;
      let service = (price * this.settings.service) * vat;
      let lightning = (elem.lightning) ? this.settings.lightning * vat : 0;
      let socialCecurity = (price * this.settings.socialsecurity) * (1+this.settings.socialCecurityVat);
      let accidentInsurance = (price * this.settings.accidentInsurance) * vat;
      let tax = price * this.settings.tax;
      let yel = price * this.settings.yel;
      let priceTemp = price - (advance + service + lightning + socialCecurity + tax + yel + accidentInsurance)
      let bilObj: BilData = {
        name: elem.name,
        fullPrice: price * (1+this.settings.vat),
        hours: priceTemp,
        advance: advance,
        service: service,
        lightning: lightning,
        accidentInsurance: accidentInsurance,
        socialsecurity: socialCecurity,
        tax: tax,
        yel: yel,
        vatReduction: ((this.settings.lightning * this.settings.vat) + (socialCecurity * this.settings.socialCecurityVat) + ((service * this.settings.vat) + (lightning * this.settings.vat) + (advance * this.settings.vat) + (accidentInsurance * this.settings.vat)))
      }
      this.dataSourceArr.push(bilObj);
      this.bilData.next(this.dataSourceArr);
    });
    this.reportData.next(this.groupdeBilsLocal);

    this.getGroupdeTickets();
  }
}
