import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable, BehaviorSubject } from 'rxjs'
import * as backend from './backend';

@Injectable({
  providedIn: 'root'
})
export class ButtonsService {

  public buttons: BehaviorSubject<any> = new BehaviorSubject(null);
  public buttonsObservable: Observable<any> = this.buttons.asObservable();
  public backendUrl = backend.conf.url;

  constructor(private http: Http) {
    this.getButtonsElements();
  }
  getButtonsElements() {
    this.http.get(this.backendUrl + '/buttons').subscribe((response) => {
      let body = JSON.parse(response['_body']);
      this.buttons.next(body.buttons);
    });
  }
}
