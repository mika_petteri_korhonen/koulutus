import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable, BehaviorSubject } from 'rxjs'
import * as backend from './backend';

@Injectable({
  providedIn: 'root'
})
export class ProductPricesService {
  public prices: BehaviorSubject<any> = new BehaviorSubject(null);
  public pricesObservable: Observable<any> = this.prices.asObservable();
  public backendUrl = backend.conf.url;
  
  constructor(private http: Http) {
    this.getPricesElements();
  }
  getPricesElements() {
    this.http.get(this.backendUrl + '/prices').subscribe((response) => {
      let body = JSON.parse(response['_body']);
      this.prices.next(body.prices);
    });
  }
}
