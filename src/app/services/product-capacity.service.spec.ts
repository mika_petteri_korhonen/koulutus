import { TestBed } from '@angular/core/testing';

import { ProductCapacityService } from './product-capacity.service';

describe('ProductCapacityService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProductCapacityService = TestBed.get(ProductCapacityService);
    expect(service).toBeTruthy();
  });
});
