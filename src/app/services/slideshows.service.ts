import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable, BehaviorSubject } from 'rxjs'
import * as backend from './backend';

@Injectable({
  providedIn: 'root'
})
export class SlideshowsService {
  public slideshows: BehaviorSubject<any> = new BehaviorSubject(null);
  public slideshowsObservable: Observable<any> = this.slideshows.asObservable();
  public backendUrl = backend.conf.url;

  constructor(private http: Http) {
    this.getSlideshowsElements();
  }
  getSlideshowsElements() {
    this.http.get(this.backendUrl + '/slideshows').subscribe((response) => {
      let body = JSON.parse(response['_body']);
      this.slideshows.next(body.slideshows);
    });
  }
}
