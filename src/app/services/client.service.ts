import { Injectable } from '@angular/core';
import { Customer } from '../interfaces/customer';
@Injectable({
  providedIn: 'root'
})
export class ClientService {
  public customers: Array<Customer> = [{
    id: 0,
    comppany: "Testi yritys",
    contact: [{
      name: "Matti Meikäläinen",
      phone: "+358 40 123 1234",
      address: "",
      email: ""
    }],
    address: {
      address: "Tehtaankatu 1",
      postalCode: "FI-00100",
      country: "Finland",
      city: "Helsinki"
    },
    vatId: "0245458-3"
  }];
  constructor() { }
}
