import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable, BehaviorSubject } from 'rxjs'
import * as backend from './backend';

@Injectable({
  providedIn: 'root'
})
export class ListsService {

  public lists: BehaviorSubject<any> = new BehaviorSubject(null);
  public listsObservable: Observable<any> = this.lists.asObservable();
  public backendUrl = backend.conf.url;

  constructor(private http: Http) {
    this.getButtonsElements();
  }
  getButtonsElements() {
    this.http.get(this.backendUrl + '/lists').subscribe((response) => {
      let body = JSON.parse(response['_body']);
      this.lists.next(body.lists);
    });
  }
}
