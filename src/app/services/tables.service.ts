import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable, BehaviorSubject } from 'rxjs'
import * as backend from './backend';

@Injectable({
  providedIn: 'root'
})
export class TablesService {

  public tables: BehaviorSubject<any> = new BehaviorSubject(null);
  public tablesObservable: Observable<any> = this.tables.asObservable();
  public backendUrl = backend.conf.url;

  constructor(private http: Http) {
    this.getTablesElements();
  }

  getTablesElements() {
    this.http.get(this.backendUrl + '/tables').subscribe((response) => {
      let body = JSON.parse(response['_body']);
      this.tables.next(body.tables);
    });
  }
}
