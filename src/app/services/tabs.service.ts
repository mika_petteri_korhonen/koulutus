import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable, BehaviorSubject } from 'rxjs'
import * as backend from './backend';

@Injectable({
  providedIn: 'root'
})
export class TabsService {
  public tabs: BehaviorSubject<any> = new BehaviorSubject(null);
  public tabsObservable: Observable<any> = this.tabs.asObservable();
  public backendUrl = backend.conf.url;
  
  constructor(private http: Http) {
    this.getTabElements();
  }
  getTabElements() {
    this.http.get(this.backendUrl + '/tabs/').subscribe((response) => {
      let body = JSON.parse(response['_body']);
      this.tabs.next(body.tabs);
    });
  }
}
