import { TestBed } from '@angular/core/testing';

import { AccordionsService } from './accordions.service';

describe('AccordionsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AccordionsService = TestBed.get(AccordionsService);
    expect(service).toBeTruthy();
  });
});
