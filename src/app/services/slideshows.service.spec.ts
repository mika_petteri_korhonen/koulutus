import { TestBed } from '@angular/core/testing';

import { SlideshowsService } from './slideshows.service';

describe('SlideshowsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SlideshowsService = TestBed.get(SlideshowsService);
    expect(service).toBeTruthy();
  });
});
