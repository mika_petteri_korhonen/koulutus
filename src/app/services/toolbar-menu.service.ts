import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable, BehaviorSubject } from 'rxjs'
import * as backend from './backend';

@Injectable({
  providedIn: 'root'
})
export class ToolbarMenuService {
  public toolbarMenus: BehaviorSubject<any> = new BehaviorSubject(null);
  public toolbarMenusObservable: Observable<any> = this.toolbarMenus.asObservable();
  public backendUrl = backend.conf.url;

  constructor(private http: Http) {
    this.getToolbarsElements();
  }
  getToolbarsElements() {
    this.http.get(this.backendUrl + '/toolbarMenus').subscribe((response) => {
      let body = JSON.parse(response['_body']);
      this.toolbarMenus.next(body.toolbarMenus);
    });
  }
}
