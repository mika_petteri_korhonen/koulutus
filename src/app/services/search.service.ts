import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class SearchService {
  public searchCriteriasObj = {};
  public searchArr = [];
  public search: BehaviorSubject<any> = new BehaviorSubject(null);
  public searchObservable: Observable<any> = this.search.asObservable();
  public searchCriterias: BehaviorSubject<any> = new BehaviorSubject(null);
  public searchCriteriasObservable: Observable<any> = this.searchCriterias.asObservable();

  constructor() { }

  updateSearch(obj) {
    this.searchArr = obj;
    this.search.next(obj);
  }
  updateSearchCriterias(obj) {
    this.searchCriterias.next(obj);
    this.searchCriteriasObj = obj;
  }
}
