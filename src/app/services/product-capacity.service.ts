import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable, BehaviorSubject } from 'rxjs'
import * as backend from './backend';

@Injectable({
  providedIn: 'root'
})
export class ProductCapacityService {
  public capacity: BehaviorSubject<any> = new BehaviorSubject(null);
  public capacityObservable: Observable<any> = this.capacity.asObservable();
  public backendUrl = backend.conf.url;
  
  constructor(private http: Http) {
    this.getCapacityElements();
  }
  getCapacityElements() {
    this.http.get(this.backendUrl + '/capacity').subscribe((response) => {
      let body = JSON.parse(response['_body']);
      this.capacity.next(body.capacity);
    });
  }
}
