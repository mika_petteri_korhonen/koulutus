import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable, BehaviorSubject } from 'rxjs'
import * as backend from './backend';

@Injectable({
  providedIn: 'root'
})
export class ElementsService {

  public elements: BehaviorSubject<any> = new BehaviorSubject(null);
  public elementsObservable: Observable<any> = this.elements.asObservable();
  public backendUrl = backend.conf.url;
  
  constructor(private http: Http) {
    this.getElements(1);
  }

  getElements(pageId) {
    this.http.get(this.backendUrl + '/elements/' + pageId).subscribe((response) => {
      let body = JSON.parse(response['_body']);
      this.elements.next(body.elements);
    });
  }
}
