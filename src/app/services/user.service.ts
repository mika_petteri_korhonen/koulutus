import { Injectable } from '@angular/core';
import { User, References, BankAccounts } from '../interfaces/user';
import * as FinnishBankUtils from 'finnish-bank-utils';
@Injectable({
  providedIn: 'root'
})
export class UserService {
  /*
  Viite: https://fi.wikipedia.org/wiki/Tilisiirto#Viitenumero | 14.9.2018 16:22:00
  Esimerkki kansainvälisen viitenumeron laskemisesta suomalaisesta viitenumerosta
Suomalainen viitenumero: 1000371

Lisätään numerosarja 271500, jolloin saadaan 1000371271500.

1000371271500 modulo 97 = 73

98 - 73 = 25

Kansainväliseksi viitenumeroksi saadaan: RF251000371
  */
  public references: Array<References> = [{
    //reference: "00 0000 0000 0000 0000 0000 0000 00" // Fails Check
    reference: FinnishBankUtils.generateFinnishRefNumber() // Automatic random ref
  }];
  public bankAccounts: Array<BankAccounts> = [{
    name: "S-Pankki",
    //iban: "FI00 0000 0000 0000 00",
    iban: FinnishBankUtils.generateFinnishIBAN(),
    swift: "AA AAA AAA AA"
  }, {
    name: "Nordea",
    //iban: "FI00 0000 0000 0000 00",
    iban: FinnishBankUtils.generateFinnishIBAN(),
    swift: "AA AAA AAA AA"
  }];
  public user: User = {
    name: "Mika Petteri Korhonen",
    address: "Porttitie 9 F 64",
    postalNumber: "FI-00940",
    city: "Helsinki",
    country: "Finland",
    bankConnections: this.bankAccounts,
    invoiceReferences: this.references,
    vatId: "0245458-3",
    phoneNumber: "(+358) 46 557 2767",
    userName: 'mika.korhonen'
  };
  constructor() { }
  /** Luhn mod 10 checksum algorithm https://en.wikipedia.org/wiki/Luhn_algorithm */
  luhnMod10(value) {
    let sum = 0
    for (let i = 0; i < value.length; i++) {
      const multiplier = (i % 2 === 0) ? 2 : 1
      let add = multiplier * parseInt(value[i], 10)
      if (add >= 10) {
        add -= 9
      }
      sum += add
    }
    const mod10 = sum % 10
    return mod10 === 0 ? mod10 : 10 - mod10
  }
}
