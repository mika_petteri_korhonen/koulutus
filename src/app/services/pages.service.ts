import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable, BehaviorSubject } from 'rxjs'
import * as backend from './backend';

@Injectable({
  providedIn: 'root'
})
export class PagesService {

  public pages: BehaviorSubject<any> = new BehaviorSubject(null);
  public pagesObservable: Observable<any> = this.pages.asObservable();
  public backendUrl = backend.conf.url;
  
  constructor(private http: Http) {
    this.getPagesElements();
  }
  getPagesElements() {
    this.http.get(this.backendUrl + '/pages').subscribe((response) => {
      let body = JSON.parse(response['_body']);
      this.pages.next(body.pages);
    });
  }
}
