import { Component, OnInit, Injectable } from '@angular/core';
import {FormControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {BehaviorSubject, merge, Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {CollectionViewer, SelectionChange} from '@angular/cdk/collections';
import {FlatTreeControl} from '@angular/cdk/tree';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: []
})
export class AppComponent {
  constructor() {
  }

  ngOnInit() {
  }
}
