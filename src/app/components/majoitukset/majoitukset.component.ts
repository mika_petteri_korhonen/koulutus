import { Component, OnInit } from '@angular/core';
import { SearchService } from '../../services/search.service';
import { ActivatedRoute } from '@angular/router';
import { ProductCapacityService } from '../../services/product-capacity.service';
import { ProductPricesService } from '../../services/product-prices.service';
import { ProductKohdeService } from '../../services/product-kohde.service';
import { Subject } from 'rxjs';
import { addDays } from 'date-fns';
import {
  CalendarEvent,
  CalendarEventTimesChangedEvent
} from 'angular-calendar';
export const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA'
  }
};
@Component({
  selector: 'app-majoitukset',
  templateUrl: './majoitukset.component.html',
  styleUrls: ['./majoitukset.component.scss']
})
export class MajoituksetComponent implements OnInit {
  view: string = 'month';

  viewDate: Date = new Date();

  events: CalendarEvent[] = [
    {
      title: 'Resizable event',
      color: colors.yellow,
      start: addDays(new Date(), 1.5),
      end: addDays(new Date(), 2.5), // an end date is always required for resizable events to work
      resizable: {
        beforeStart: true, // this allows you to configure the sides the event is resizable from
        afterEnd: true
      }
    },
    {
      title: 'A non resizable event',
      color: colors.blue,
      start: new Date(),
      end: addDays(new Date(), 1)
    }
  ];

  refresh: Subject<any> = new Subject();
  public searchedItems = [];
  private kohdeCode = "";
  private kohdeTieto = {
    kohdeCode: "",
    alkupvm: "",
    loppupvm: "",
    henkilot: ""
  };
  public priceTable = ['date', 'price'];
  public products = [];
  public searchedProducts = [];
  public kohdeDb = [];
  public pricesDb = [];
  public capacityDb = [];
  public searchObj: any;
  constructor(private capacity: ProductCapacityService, 
    private prices: ProductPricesService,
    private searchSrv: SearchService,
    private route: ActivatedRoute,
    private kohde: ProductKohdeService) { 
      this.searchSrv.searchCriteriasObservable.subscribe((response) => {
        if(response != null) {
          console.log(response);
          this.searchObj = response;
        }
      });
      this.kohde.kohdeObservable.subscribe((response) => {
        if(response != null) {
          console.log(response);
          this.kohdeDb = response;
          if(this.searchSrv.searchArr.length === 0) {
            console.log("täällä");
            this.search();
            this.searchSrv.searchArr.forEach((elem, ind) => {
              if(elem.kohdeCode === this.kohdeTieto.kohdeCode) {
                console.log(elem);
                this.products.push(elem);
              }
            });
          } else {
          }
        }
      });
      this.prices.pricesObservable.subscribe((response) => {
        if(response != null) {
          console.log(response);
          this.pricesDb = response;
        }
      });
      this.capacity.capacityObservable.subscribe((response) => {
        if(response != null) {
          console.log(response);
          this.capacityDb = response;
        }
      });
      this.route.params.subscribe(params => {
        this.kohdeTieto.kohdeCode = params['kohdeCode'];
        this.kohdeTieto.alkupvm = params['alkupvm'];
        this.kohdeTieto.loppupvm = params['loppupvm'];
        this.kohdeTieto.henkilot = params['henkilot'];
        console.log(this.kohdeTieto);
      });
  }

  ngOnInit() {
    this.searchSrv.searchArr.forEach((elem, ind) => {
      if(elem.kohdeCode === this.kohdeTieto.kohdeCode) {
        console.log(elem);
        this.products.push(elem);
      }
    });
  }

  eventTimesChanged({
    event,
    newStart,
    newEnd
  }: CalendarEventTimesChangedEvent): void {
    event.start = newStart;
    event.end = newEnd;
    this.refresh.next();
  }
  search() {
    console.log(this.kohdeDb);
    this.searchedProducts = [];
    this.kohdeDb.forEach((elem, ind) => {
      if(elem.kohdeTyyppi === 'MAJ') {
        elem.prices = [];
        elem.capacity = [];
        elem.table = [];
        this.searchedProducts.push(elem);
      }
    });
    this.searchedProducts.forEach((elem, ind) => {
      this.pricesDb.forEach((elem2, ind2) => {
        if(elem.kohdeTyyppi === elem2.productCode && elem.kohdeCode === elem2.kohdeCode) {
          let finDate = elem2.date.split('.');
          let realDate = new Date(finDate[2] + '-' + finDate[1] + '-' + finDate[0]);
          let startDateFin = this.kohdeTieto.alkupvm.split('.');
          let startDate = new Date(startDateFin[2] + '-' + startDateFin[1] + '-' + startDateFin[0]);
          let endDateFin = this.kohdeTieto.loppupvm.split('.');
          let endDate = new Date(endDateFin[2] + '-' + endDateFin[1] + '-' + endDateFin[0]);
          if(startDate.getTime() <= realDate.getTime()) {
            if(startDate.getTime() === realDate.getTime() || endDate.getTime() >= realDate.getTime()) {
              elem.prices.push(elem2);
              elem.table.push({
                date: elem2.date,
                price: elem2.price
              });
            }
          }
        }
      });
      this.capacityDb.forEach((elem3, ind3) => {
        if(elem.kohdeTyyppi === elem3.productCode && elem.kohdeCode === elem3.kohdeCode) {
          let finDate = elem3.date.split('.');
          let realDate = new Date(finDate[2] + '-' + finDate[1] + '-' + finDate[0]);
          let startDateFin = this.kohdeTieto.alkupvm.split('.');
          let startDate = new Date(startDateFin[2] + '-' + startDateFin[1] + '-' + startDateFin[0]);
          let endDateFin = this.kohdeTieto.loppupvm.split('.');
          let endDate = new Date(endDateFin[2] + '-' + endDateFin[1] + '-' + endDateFin[0]);
          if(startDate.getTime() <= realDate.getTime()) {
            if(startDate.getTime() === realDate.getTime() || endDate.getTime() >= realDate.getTime()) {
              elem.capacity.push(elem3);
            }
          }
        }
      });
    });
    this.searchSrv.updateSearch(this.searchedProducts);
    console.log(this.searchedProducts);
  }
}
