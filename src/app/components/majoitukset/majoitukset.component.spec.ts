import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MajoituksetComponent } from './majoitukset.component';

describe('MajoituksetComponent', () => {
  let component: MajoituksetComponent;
  let fixture: ComponentFixture<MajoituksetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MajoituksetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MajoituksetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
