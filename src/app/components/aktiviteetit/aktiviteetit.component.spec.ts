import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AktiviteetitComponent } from './aktiviteetit.component';

describe('AktiviteetitComponent', () => {
  let component: AktiviteetitComponent;
  let fixture: ComponentFixture<AktiviteetitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AktiviteetitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AktiviteetitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
