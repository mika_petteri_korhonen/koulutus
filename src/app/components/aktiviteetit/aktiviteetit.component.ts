import { Component, OnInit } from '@angular/core';
import { SearchService } from '../../services/search.service';
import { ActivatedRoute } from '@angular/router';
import { ProductCapacityService } from '../../services/product-capacity.service';
import { ProductPricesService } from '../../services/product-prices.service';
import { ProductKohdeService } from '../../services/product-kohde.service';

@Component({
  selector: 'app-aktiviteetit',
  templateUrl: './aktiviteetit.component.html',
  styleUrls: ['./aktiviteetit.component.scss']
})
export class AktiviteetitComponent implements OnInit {
  public searchedItems = [];
  private kohdeCode = "";
  private kohdeTieto = {
    kohdeCode: "",
    alkupvm: "",
    loppupvm: "",
    henkilot: ""
  };
  public priceTable = ['date', 'price'];
  public products = [];
  public searchedProducts = [];
  public kohdeDb = [];
  public pricesDb = [];
  public capacityDb = [];
  public searchObj: any;
  constructor(private capacity: ProductCapacityService, 
    private prices: ProductPricesService,
    private searchSrv: SearchService,
    private route: ActivatedRoute,
    private kohde: ProductKohdeService) { 
      this.searchSrv.searchCriteriasObservable.subscribe((response) => {
        if(response != null) {
          this.searchObj = response;
        }
      });
      this.kohde.kohdeObservable.subscribe((response) => {
        if(response != null) {
          this.kohdeDb = response;
          if(this.searchSrv.searchArr.length === 0) {
            this.search();
            this.searchSrv.searchArr.forEach((elem, ind) => {
              if(elem.kohdeCode === this.kohdeTieto.kohdeCode) {
                this.products.push(elem);
              }
            });
          } else {
          }
        }
      });
      this.prices.pricesObservable.subscribe((response) => {
        if(response != null) {
          this.pricesDb = response;
        }
      });
      this.capacity.capacityObservable.subscribe((response) => {
        if(response != null) {
          this.capacityDb = response;
        }
      });
      this.route.params.subscribe(params => {
        this.kohdeTieto.kohdeCode = params['kohdeCode'];
        this.kohdeTieto.alkupvm = params['alkupvm'];
        this.kohdeTieto.loppupvm = params['loppupvm'];
        this.kohdeTieto.henkilot = params['henkilot'];
      });
  }

  ngOnInit() {
    this.searchSrv.searchArr.forEach((elem, ind) => {
      if(elem.kohdeCode === this.kohdeTieto.kohdeCode) {
        this.products.push(elem);
      }
    });
  }


  search() {
    this.searchedProducts = [];
    this.kohdeDb.forEach((elem, ind) => {
      if(elem.kohdeTyyppi === 'AKT') {
        elem.prices = [];
        elem.capacity = [];
        elem.table = [];
        this.searchedProducts.push(elem);
      }
    });
    this.searchedProducts.forEach((elem, ind) => {
      this.pricesDb.forEach((elem2, ind2) => {
        if(elem.kohdeTyyppi === elem2.productCode && elem.kohdeCode === elem2.kohdeCode) {
          let finDate = elem2.date.split('.');
          let realDate = new Date(finDate[2] + '-' + finDate[1] + '-' + finDate[0]);
          let startDateFin = this.kohdeTieto.alkupvm.split('.');
          let startDate = new Date(startDateFin[2] + '-' + startDateFin[1] + '-' + startDateFin[0]);
          let endDateFin = this.kohdeTieto.loppupvm.split('.');
          let endDate = new Date(endDateFin[2] + '-' + endDateFin[1] + '-' + endDateFin[0]);
          if(startDate.getTime() <= realDate.getTime()) {
            if(startDate.getTime() === realDate.getTime() || endDate.getTime() >= realDate.getTime()) {
              elem.prices.push(elem2);
              elem.table.push({
                date: elem2.date,
                price: elem2.price
              });
            }
          }
        }
      });
      this.capacityDb.forEach((elem3, ind3) => {
        if(elem.kohdeTyyppi === elem3.productCode && elem.kohdeCode === elem3.kohdeCode) {
          let finDate = elem3.date.split('.');
          let realDate = new Date(finDate[2] + '-' + finDate[1] + '-' + finDate[0]);
          let startDateFin = this.kohdeTieto.alkupvm.split('.');
          let startDate = new Date(startDateFin[2] + '-' + startDateFin[1] + '-' + startDateFin[0]);
          let endDateFin = this.kohdeTieto.loppupvm.split('.');
          let endDate = new Date(endDateFin[2] + '-' + endDateFin[1] + '-' + endDateFin[0]);
          if(startDate.getTime() <= realDate.getTime()) {
            if(startDate.getTime() === realDate.getTime() || endDate.getTime() >= realDate.getTime()) {
              elem.capacity.push(elem3);
            }
          }
        }
      });
    });
    this.searchSrv.updateSearch(this.searchedProducts);
  }
}
