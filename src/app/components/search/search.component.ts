import { Component, OnInit } from '@angular/core';
import { ProductCapacityService } from '../../services/product-capacity.service';
import { ProductPricesService } from '../../services/product-prices.service';
import { ProductKohdeService } from '../../services/product-kohde.service';
import { ProductsService } from '../../services/products.service';
import { SearchService } from '../../services/search.service';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
  providers: [
    // The locale would typically be provided on the root module of your application. We do it at
    // the component level here, due to limitations of our example generation script.
    {provide: MAT_DATE_LOCALE, useValue: 'fi-FI'},

    // `MomentDateAdapter` and `MAT_MOMENT_DATE_FORMATS` can be automatically provided by importing
    // `MatMomentDateModule` in your applications root module. We provide it at the component level
    // here, due to limitations of our example generation script.
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS},
  ]
})
export class SearchComponent implements OnInit {
  public minDate = new Date();
  public minDate2 = new Date(new Date().setDate(new Date().getDate() + 1));
  public searchObj = {
    tripType: "",
    startDate: this.minDate,
    endDate: this.minDate2,
    persons: ""
  };
  public kohdeDb = [];
  public pricesDb = [];
  public capacityDb = [];
  public searchedProducts = [];
  constructor(private capacity: ProductCapacityService, 
    private prices: ProductPricesService,
    private kohde: ProductKohdeService,
    private products: ProductsService,
    private searchSrv: SearchService) {
      this.kohde.kohdeObservable.subscribe((response) => {
        if(response != null) {
          this.kohdeDb = response;
        }
      });
      this.prices.pricesObservable.subscribe((response) => {
        if(response != null) {
          this.pricesDb = response;
        }
      });
      this.capacity.capacityObservable.subscribe((response) => {
        if(response != null) {
          this.capacityDb = response;
        }
      });
    }

  ngOnInit() {
  }

  search(event) {
    event.stopPropagation();
    event.preventDefault();
    this.searchSrv.updateSearchCriterias(this.searchObj);
    this.searchedProducts = [];
    this.kohdeDb.forEach((elem, ind) => {
      if(elem.kohdeTyyppi === this.searchObj.tripType) {
        elem.prices = [];
        elem.capacity = [];
        elem.table = [];
        this.searchedProducts.push(elem);
      }
    });
    this.searchedProducts.forEach((elem, ind) => {
      this.pricesDb.forEach((elem2, ind2) => {
        if(elem.kohdeTyyppi === elem2.productCode && elem.kohdeCode === elem2.kohdeCode) {
          let finDate = elem2.date.split('.');
          let realDate = new Date(finDate[2] + '-' + finDate[1] + '-' + finDate[0]);
          let startDate = new Date(this.searchObj.startDate);
          let endDate = new Date(this.searchObj.endDate);
          if(startDate.getTime() <= realDate.getTime()) {
            if(startDate.getTime() === realDate.getTime() || endDate.getTime() >= realDate.getTime()) {
              elem.prices.push(elem2);
              elem.table.push({
                date: elem2.date,
                price: elem2.price
              });
            }
          }
        }
      });
      this.capacityDb.forEach((elem3, ind3) => {
        if(elem.kohdeTyyppi === elem3.productCode && elem.kohdeCode === elem3.kohdeCode) {
          let finDate = elem3.date.split('.');
          let realDate = new Date(finDate[2] + '-' + finDate[1] + '-' + finDate[0]);
          let startDate = new Date(this.searchObj.startDate);
          let endDate = new Date(this.searchObj.endDate);
          if(startDate.getTime() <= realDate.getTime()) {
            if(startDate.getTime() === realDate.getTime() || endDate.getTime() >= realDate.getTime()) {
              elem.capacity.push(elem3);
            }
          }
        }
      });
    });
    this.searchSrv.updateSearch(this.searchedProducts);
  }
  activities(event) {
    this.searchObj.tripType = event.value;
  }
  changePersons(event) {
    this.searchObj.persons = event.value;
  }
  changeEndDate(event) {
    this.searchObj.endDate = event.value;
  }
  changeStartDate(event) {
    let startDate = new Date(event.value);
    startDate.setDate(startDate.getDate() + 1);
    this.minDate2 = startDate;
    this.searchObj.startDate = event.value;
  }
}
