import { Component, OnInit, Injectable } from '@angular/core';
import { ElementsService } from '../../services/elements.service';
import { TabsService } from '../../services/tabs.service';
import { TablesService } from '../../services/tables.service';
import { CardsService } from '../../services/cards.service';
import { ButtonsService } from '../../services/buttons.service';
import { ListsService } from '../../services/lists.service';
import { AccordionsService } from '../../services/accordions.service';
import { ToolbarMenuService } from '../../services/toolbar-menu.service';
import { SlideshowsService } from '../../services/slideshows.service';
import { ShoppingCartService } from '../../services/shopping-cart.service';
import { ProductsService } from '../../services/products.service';
import { Sort, PageEvent} from '@angular/material';
import {FormControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {BehaviorSubject, merge, Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {CollectionViewer, SelectionChange} from '@angular/cdk/collections';
import {FlatTreeControl} from '@angular/cdk/tree';
import { SearchService } from '../../services/search.service';

@Component({
  selector: 'app-combined-page',
  templateUrl: './combined-page.component.html',
  styleUrls: ['./combined-page.component.scss'],
  providers: []
})
export class CombinedPageComponent implements OnInit {
  public myColor = "#cecece";
  public favoriteSeason: string;
  public seasons: string[] = ['Winter', 'Spring', 'Summer', 'Autumn'];
  public date = new FormControl(new Date());
  public serializedDate = new FormControl((new Date()).toISOString());
  public disabled = false;
  public showFiller = false;
  public stateCtrl = new FormControl();
  isLinear = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  public elements = [];
  public tabs = [];
  public displayedColumns: string[] = [];
  public dataSource = [];
  public dataSourceOrig = [];
  public tables = [];
  public sortedData = [];
  public length = 100;
  public pageSize = 1;
  public pageSizeOptions: number[] = [];
  public pageEvent: PageEvent;
  public showPaginator = false;
  public pageIndex = 0;
  public cards = [];
  public buttons = [];
  public lists = [];
  public accordions = [];
  public toolbarMenus = [];
  public slideshows = [];
  public shoppingCart = [];
  public products = [];
  public invoiceOpen = false;
  public imageSources = ['/assets/free.jpg','/assets/free1.jpg','/assets/free2.jpeg','/assets/free3.jpeg','/assets/free4.jpeg'];

  public toolbarToggler = false;
  public priceTable = ['date', 'price'];
  public searchObj = {};
  constructor(private elementService: ElementsService, 
    private tabsService: TabsService, 
    private tablesService: TablesService,
    private cardsService: CardsService,
    private buttonsService: ButtonsService,
    private listsService: ListsService,
    private accordionsService: AccordionsService,
    private toolbarMenuService: ToolbarMenuService,
    private slideshowsService: SlideshowsService,
    private shoppingCartService: ShoppingCartService,
    private productsService: ProductsService,
    private _formBuilder: FormBuilder,
    private searchSrv: SearchService) {
    this.searchSrv.searchCriteriasObservable.subscribe((response) => {
      if(response != null) {
        this.searchObj = response;
      }
    });
    this.searchSrv.searchObservable.subscribe((response) => {
      if(response != null) {
        this.products = response;
      }
    });
    /*
    this.productsService.productsObservable.subscribe((results) => {
      if(results != null) {
        console.log(results);
        this.products = results;
      }
    });
    */
    this.shoppingCartService.shoppingCartObservable.subscribe((results) => {
      if(results != null) {
        this.shoppingCart = results;
      }
    });
    this.slideshowsService.slideshowsObservable.subscribe((results) => {
      if(results != null) {
        this.slideshows = results;
      }
    });
    this.toolbarMenuService.toolbarMenusObservable.subscribe((results) => {
      if(results != null) {
        this.toolbarMenus = results;
      }
    });
    this.accordionsService.accordionsObservable.subscribe((results) => {
      if(results != null) {
        this.accordions = results;
      }
    });
    this.listsService.listsObservable.subscribe((results) => {
      if(results != null) {
        this.lists = results;
      }
    });
    this.buttonsService.buttonsObservable.subscribe((results) => {
      if(results != null) {
        this.buttons = results;
      }
    });
    this.cardsService.cardsObservable.subscribe((results) => {
      if(results != null) {
        this.cards = results;
      }
    });
    this.elementService.elementsObservable.subscribe((results) => {
      if(results != null) {
        this.elements = results;
      }
    });
    this.tabsService.tabsObservable.subscribe((results) => {
      if(results != null) {
        this.tabs = results;
      }
    });
    this.tablesService.tablesObservable.subscribe((results) => {
      if(results != null) {
        this.tables = results;
        this.tables.forEach((elem, ind) => {
          if(this.displayedColumns.indexOf(elem.colName) === -1) {
            this.displayedColumns.push(elem.colName);
          }
        });
        const tempObj = this.displayedColumns.reduce((o, key) => ({ ...o, [key]: ""}), {})
        
        this.tables.forEach((elem, ind) => {
          if(parseInt(elem.tableId, 10) === 1) {
            tempObj[elem.colName] = elem.value;
            if(this.displayedColumns.indexOf(elem.colName) === -1) {
              this.displayedColumns.push(elem.colName);
            }
            if((ind % (Object.keys(tempObj).length)) === 3) {
              this.dataSource.push(JSON.parse(JSON.stringify(tempObj)));
            }
          }
        });
        this.dataSourceOrig = this.dataSource;
        for(let i = 0; i < this.dataSourceOrig.length; i++) {
          this.pageSizeOptions.push(i+1);
        }
        this.setPageSize();
        this.showPaginator = true;
      }
    });
  }

  ngOnInit() {
    this.sortedData = this.dataSourceOrig.slice();
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
  }
  toProductPage(event, product) {
    event.stopPropagation();
    event.preventDefault();
    console.log(product);
  }
  openToolbar(event) {
    event.preventDefault();
    this.toolbarToggler = !this.toolbarToggler;
  }
  openMenu(event) {
    event.stopPropagation();
    event.preventDefault();
  }
  openInvoice(event) {
    event.stopPropagation();
    event.preventDefault();
    this.invoiceOpen = !this.invoiceOpen;
  }
  addProduct(event, productCode) {
    console.log(event, parseInt(productCode));
    this.shoppingCartService.addProductToCart(this.products[parseInt(productCode)]);
  }
  warn(event) {
    console.log("warn", event);
  }
  test(event) {
    console.log("test", event);
  }
  test2(event) {
    console.log("test2", event);
  }
  changePage(event) {
    console.log(event);
    this.pageSize = event.pageSize;
    this.pageIndex = event.pageIndex;
    this.setPageSize();
  }
  setPageSize() {
    this.dataSource = [];
    this.dataSourceOrig.forEach((elem, ind) => {
      let counter = ind/this.pageSize;
      if(this.pageIndex === ind && counter === this.pageIndex) {
        this.dataSource.push(elem);
      } else {
        if(ind < this.pageSize && counter === this.pageIndex) {
          this.dataSource.push(elem);
        }
        if(counter < 1 && counter != 0 && (ind/this.pageSize) != 0.5) {
          this.dataSource.push(elem);
        } else {
          if(counter === 0.5 && this.pageIndex === 0) {
            this.dataSource.push(this.dataSourceOrig[ind]);
          } else {
            if(counter === this.pageIndex) {
              this.dataSource.push(this.dataSourceOrig[ind]);
            }
          }
        }
      }
    });
  }
  setPageSizeOptions(setPageSizeOptionsInput: string) {
    console.log(setPageSizeOptionsInput);
    this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
  }
  sortData(sort: Sort) {
    const data = this.dataSource.slice();
    if (!sort.active || sort.direction === '') {
      this.dataSource = data;
      return;
    }

    this.dataSource = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      return compare(a[sort.active], b[sort.active], isAsc);
    });
  }
}

function compare(a: number | string, b: number | string, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}