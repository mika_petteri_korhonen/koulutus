import { Component, OnInit, Input } from '@angular/core';
import { TicketsService } from '../../services/tickets.service';
import { Settings } from '../../interfaces/settings';
import * as jspdf from 'jspdf';  
import html2canvas from 'html2canvas';
import { Http } from '@angular/http';
import * as JsBarcode from 'jsbarcode';
import * as FinnishBankUtils from 'finnish-bank-utils';
import { ClientService } from '../../services/client.service';
import { UserService } from '../../services/user.service';
import { InvoiceService } from '../../services/invoice.service';
import { Customer } from '../../interfaces/customer';
import { User } from '../../interfaces/user';
import { Invoice } from '../../interfaces/invoice';
import { ShoppingCartService } from '../../services/shopping-cart.service';
@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.scss']
})
export class InvoiceComponent implements OnInit {
  @Input() customerId: number;
  @Input() invoiceId: number;
  @Input() billedHours: number;
  public dataSource = [];
  public image;
  public input;
  public rows = [];
  public id = "";
  public invoiceSepaQrUrl = "";
  public settings: Settings;
  public user: User;
  public customer: Customer;
  public invoice: Invoice;
  public buttonsVisible: boolean = true;
  constructor(
    private ticketService: TicketsService,
    private http: Http,
    private clientService: ClientService,
    private userService: UserService,
    private invoiceService: InvoiceService,
    private shoppingCartService: ShoppingCartService) {
    this.user = this.userService.user;
    this.id = this.getId('invoice');
    this.settings = this.ticketService.settings;
    this.invoice = this.invoiceService.invoices[0];
    this.customer = this.clientService.customers[0];
    console.log(this.invoice);
    console.log(this.customer);
    this.shoppingCartService.shoppingCartObservable.subscribe((response) => {
      if(response != null) {
        console.log(response);
        this.dataSource = response;
      }
    });
  }

  ngOnInit() {
    this.fillOutInvoice();
  }
  fillOutInvoice() {
    this.invoice.price = 0;
    this.invoice.fullPrice = 0;
    console.log(this.dataSource);
    this.dataSource.forEach((elem, ind) => {
      this.rows.push({
        id: this.rows.length,
        item: elem.project,
        description: "Ohjelmointityöt",
        rate: parseInt(elem.price, 10),
        quantity: parseInt(elem.quantity, 10),
        price: parseInt(elem.price, 10) * parseInt(elem.quantity, 10),
        priceWithVat: (parseInt(elem.price, 10) * parseInt(elem.quantity, 10)) * (1 + parseFloat(elem.vatNumerological))
      });
      this.invoice.price += parseInt(elem.price, 10) * parseInt(elem.quantity, 10);
      this.invoice.fullPrice += (parseInt(elem.price, 10) * parseInt(elem.quantity, 10)) * (1 + parseFloat(elem.vatNumerological));
    });
    this.invoice.vat = this.invoice.fullPrice - this.invoice.price;
    this.invoice.due = Math.round(this.invoice.fullPrice - this.invoice.paid);
    setTimeout(() => {
      this.createSepaQR(this.user.name, this.user.bankConnections[0].iban, this.invoice.due, this.id);
      this.createSeapBarcode(this.user.bankConnections[0].iban, this.invoice.due, this.user.invoiceReferences[0].reference, this.getFinishDate());
    }, 1000);
  }
  createSeapBarcode(iban, sum, reference, date) {
    var tempObj = {
      iban: FinnishBankUtils.formatFinnishIBAN(iban),
      sum: sum, 
      reference: reference,
      date: date
    };
    var virtualBarcode = FinnishBankUtils.formatFinnishVirtualBarCode(tempObj);
    JsBarcode('section.' + this.id + ' .barcode', virtualBarcode, { 
      format: "CODE128"
    })
  }
  getFinishDate() {
    let refDate = new Date()
    var day = refDate.getDate(); // Get the day 
    var month = refDate.getMonth() + 1; // The month start to zero
    var year = refDate.getFullYear();// Get the year 
    return day + '.' + month + '.' + year;
  }
  createSepaQR(name, iban, amount, invoiceId) {
    var qrUrl = this.ticketService.backendUrl + "/qr/" + name + "/" + iban + "/" + amount + "/" + invoiceId;
    this.http.get(qrUrl)
    .subscribe((data) => {
      let body = JSON.parse(data['_body']);
      if(body.created) {
        this.invoiceSepaQrUrl = this.ticketService.backendUrl + '/sepa/' + invoiceId + '.png';
      }
    });
  }
  getId(name): string {
    return (name + '_' + this.guidGenerator()).toLowerCase();
  }
  guidGenerator() {
    let S4 = () => {
      return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
    };
    return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4());
  }
  onClick(e, row) {
    var element = e.target.querySelector('[contenteditable]'), row;

    element && e.target != document.documentElement && e.target != document.body && element.focus();

    if (e.target.matches('.add')) {
      this.generateTableRow();
    }
    else if (e.target.className == 'cut') {
      this.rows.forEach((elem, ind) => {
        let elemString = JSON.stringify(elem);
        let rowString = JSON.stringify(row);
        if(elemString === rowString) {
          this.rows.splice(ind, 1);
        }
      });
    }
    this.updateInvoice();
  }

  onEnterCancel(e) {
    e.preventDefault();

    this.image.classList.add('hover');
  }

  onLeaveCancel(e) {
    e.preventDefault();

    this.image.classList.remove('hover');
  }

  onFileInput(e) {
    this.image.classList.remove('hover');

    var reader = new FileReader(),
    files = e.dataTransfer ? e.dataTransfer.files : e.target.files,
    i = 0;

    reader.onload = this.onFileLoad;

    while (files[i]) reader.readAsDataURL(files[i++]);
  }

  onFileLoad(e) {
    var data = e.target.result;

    this.image.src = data;
  }

  generateTableRow() {
    this.rows.push({
      id: this.rows.length,
      item: "Ohjelmointityöt",
      description: "Front end työt",
      rate: 25,
      quantity: 1,
      price: 25 * 1
    });
  }

  parseFloatHTML(element) {
    return parseFloat(element.innerHTML.replace(/[^\d\.\-]+/g, '')) || 0;
  }

  parsePrice(number) {
    return number.toFixed(2).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1,');
  }

  /* Update Number
  /* ========================================================================== */

  updateNumber(e) {
    var activeElement = document.activeElement,
    value = parseFloat(activeElement.innerHTML),
    wasPrice = activeElement.innerHTML == this.parsePrice(this.parseFloatHTML(activeElement));

    if (!isNaN(value) && (e.keyCode == 38 || e.keyCode == 40 || e.wheelDeltaY)) {
      e.preventDefault();

      value += e.keyCode == 38 ? 1 : e.keyCode == 40 ? -1 : Math.round(e.wheelDelta * 0.025);
      value = Math.max(value, 0);

      activeElement.innerHTML = wasPrice ? this.parsePrice(value) : value;
    }

    this.updateInvoice();
  }

  /* Update Invoice
  /* ========================================================================== */

  updateInvoice() {
    var total = 0;
    var totalWithVat = 0;
    var cells, price, a, i;

    // update inventory cells
    // ======================

    for (a = document.querySelectorAll("section." + this.id + ' table.inventory tbody tr'), i = 0; a[i]; ++i) {
      // get inventory row cells
      cells = a[i].querySelectorAll('span:last-child');

      // set price as cell[2] * cell[3]
      price = this.parseFloatHTML(cells[2]) * this.parseFloatHTML(cells[3]);
      totalWithVat += this.parseFloatHTML(cells[5]);
      // add price to total
      total += price;

      // set row total
      cells[4].innerHTML = price;
    }

    // update balance cells
    // ====================

    // get balance cells
    var elementWaitPromise = new Promise((hyva, paha) => {
        var tries = 0;
        const timeout = setInterval(() => {
          var cellsCheck = document.querySelectorAll("section." + this.id + ' table.balance td:last-child span:last-child');
            if (cellsCheck.length > 0) {
                clearInterval(timeout);
                hyva(true);
            } else {
                tries += 1;
            }
            if(tries > 100) {
                paha("Rivejä ei löytynyt");
                clearInterval(timeout);
            }
        }, 1000);
    });
    elementWaitPromise.then((response) => {
      cells = document.querySelectorAll("section." + this.id + ' table.balance td:last-child span:last-child');

      // set total
      cells[0].innerHTML = total;
      cells[1].innerHTML = totalWithVat;
      cells[2].innerHTML = totalWithVat - total;
      // set balance and meta balance
      cells[4].innerHTML = document.querySelector("section." + this.id + ' table.meta tr:last-child td:last-child span:last-child').innerHTML = this.parsePrice(totalWithVat - this.parseFloatHTML(cells[3]));
      var fullAmount = (totalWithVat - this.parseFloatHTML(cells[3]));
      if(fullAmount > 0) {
        // this.createSepaQR(this.user.name, this.user.iban, fullAmount, this.id);
        // this.createSeapBarcode(this.user.iban, fullAmount, this.user.invoiceReferences[0].reference, this.getFinishDate());
      }
      // update prefix formatting
      // ========================
  
      var prefix = document.querySelector("section." + this.id + ' #prefix').innerHTML;
      for (a = document.querySelectorAll("section." + this.id + ' [data-prefix]'), i = 0; a[i]; ++i) a[i].innerHTML = prefix;
  
      // update price formatting
      // =======================
  
      for (a = document.querySelectorAll("section." + this.id + ' span[data-prefix] + span'), i = 0; a[i]; ++i) if (document.activeElement != a[i]) a[i].innerHTML = this.parsePrice(this.parseFloatHTML(a[i]));
    });
  }
  fullscreen() {
    this.captureScreen();
  }
  captureScreen() {
    this.buttonsVisible = false;
    setTimeout(() => {
      new Promise((hyva, paha) => {
        var data = document.querySelectorAll('.' + this.id)[0];
        html2canvas(data).then(canvas => {
          // Few necessary setting options  
          var imgWidth = 210;   
          var pageHeight = 298;    
          var imgHeight = canvas.height * imgWidth / canvas.width;  
          var heightLeft = imgHeight;  
      
          const contentDataURL = canvas.toDataURL('image/png').replace('image/png;', 'image/octet-stream;')
          let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF  
          var position = 0;  
          pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
          var save = pdf.save(this.id + '.pdf'); // Generated PDF
          hyva(true); 
        });  
      }).then((response) => {
        setTimeout(() => {
          this.buttonsVisible = true;
        }, 2000)
      });
    }, 500)
  }
}