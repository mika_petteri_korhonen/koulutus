import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import 'hammerjs';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDatepickerModule } from '@angular/material/datepicker';
import {MatTreeModule} from '@angular/material/tree';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { DemoUtilsModule } from './demoUtilsModule';
import { 
  MatNativeDateModule, 
  MatInputModule, 
  MatSelectModule, 
  MatAutocompleteModule, 
  MatButtonModule, 
  MatTabsModule,
  MatTableModule,
  MatCheckboxModule,
  MatToolbarModule,
  MatDialogModule,
  MatCardModule,
  MatExpansionModule,
  MAT_DIALOG_DEFAULT_OPTIONS,
  MatListModule,
  MatIconModule,
  MatProgressSpinnerModule,
  MatPaginatorModule,
  MatSortModule,
  MatButtonToggleModule,
  MatSidenavModule,
  MatRadioModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatMenuModule,
  MatDividerModule,
  MatStepperModule,
  MatBadgeModule,
  MatChipsModule,
  MatProgressBarModule,
  MatRippleModule,
  MatBottomSheetModule } from '@angular/material';
import { SlideshowModule } from 'ng-simple-slideshow';
import { CombinedPageComponent } from './components/combined-page/combined-page.component';
import { InvoiceComponent } from './components/invoice/invoice.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { SearchComponent } from './components/search/search.component';
import { MajoituksetComponent } from './components/majoitukset/majoitukset.component';
import { AktiviteetitComponent } from './components/aktiviteetit/aktiviteetit.component';

@NgModule({
  declarations: [
    AppComponent,
    CombinedPageComponent,
    InvoiceComponent,
    SearchComponent,
    MajoituksetComponent,
    AktiviteetitComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatNativeDateModule, 
    MatInputModule, 
    MatSelectModule, 
    MatAutocompleteModule, 
    MatButtonModule, 
    MatTabsModule,
    MatTableModule,
    MatCheckboxModule,
    MatToolbarModule,
    MatDialogModule,
    MatCardModule,
    MatExpansionModule,
    MatListModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatSortModule,
    MatButtonToggleModule,
    MatDatepickerModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    FormsModule,
    ReactiveFormsModule,
    MatRadioModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatMenuModule,
    MatDividerModule,
    MatStepperModule,
    MatTreeModule,
    MatBadgeModule,
    MatChipsModule,
    MatProgressBarModule,
    MatRippleModule,
    MatBottomSheetModule,
    HttpModule,
    SlideshowModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    ScrollToModule.forRoot(),
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    DemoUtilsModule
  ],
  providers: [{provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: true}}],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
