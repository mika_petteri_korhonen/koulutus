const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const fs = require('fs');
const {createWriteStream} = require('fs');
const PORT = 8080;
const createSepaQr = require('sepa-qr');
var CryptoJS = require("crypto-js");
// Siirrä salasana käyttäjäpohjaiseksi
var server = require('./server.conf');
var mysql      = require('mysql');
const app = express();
var pool  = mysql.createPool(server.conf.mysql);

app.use(cors());
app.use(bodyParser.json());

app.use('/', express.static(__dirname + '/dist/education'));

app.use('/sepa/', express.static(__dirname + '/sepa'));

app.get('/qr/:name/:iban/:amount/:remittance', (req, res) => {
    var name = req.params.name;
    var iban = req.params.iban;
    var amount = req.params.amount;
    var remittance = req.params.remittance;
    var sepa = createSepaQr({
        name: name,
        iban: iban,
        amount: parseFloat(amount), // in Euro
        remittance: remittance
    }).pipe(createWriteStream(__dirname + '/sepa/'+remittance+'.png'));
    if(sepa) {
        console.log("qr created" + remittance);
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify({ created: true }));
    }
});
app.get('/elements/:pageId', (req, res) => {
    var pageId = req.params.pageId;
    pool.getConnection(function(err, connection) {
        if (err) throw err; // not connected!
    
        // Use the connection
        connection.query('SELECT * FROM elements WHERE pageId = '+pageId+' ORDER BY elementOrder ASC;', function (error, results, fields) {
        // When done with the connection, release it.
        connection.release();
    
        // Handle error after the release.
        if (error) throw error;
    
        res.send(JSON.stringify({ elements: results }));
        // Don't use the connection here, it has been returned to the pool.
        });
    });
});

app.get('/tabs', (req, res) => {
    pool.getConnection(function(err, connection) {
        if (err) throw err; // not connected!
    
        // Use the connection
        connection.query('SELECT * FROM tabs ORDER BY tabsOrder ASC;', function (error, results, fields) {
        // When done with the connection, release it.
        connection.release();
    
        // Handle error after the release.
        if (error) throw error;
    
        res.send(JSON.stringify({ tabs: results }));
        // Don't use the connection here, it has been returned to the pool.
        });
    });
});
app.get('/tables', (req, res) => {
    pool.getConnection(function(err, connection) {
        if (err) throw err; // not connected!
    
        // Use the connection
        connection.query('SELECT * FROM tables;', function (error, results, fields) {
        // When done with the connection, release it.
        connection.release();
    
        // Handle error after the release.
        if (error) throw error;
    
        res.send(JSON.stringify({ tables: results }));
        // Don't use the connection here, it has been returned to the pool.
        });
    });
});
app.get('/cards', (req, res) => {
    pool.getConnection(function(err, connection) {
        if (err) throw err; // not connected!
    
        // Use the connection
        connection.query('SELECT * FROM cards;', function (error, results, fields) {
        // When done with the connection, release it.
        connection.release();
    
        // Handle error after the release.
        if (error) throw error;
    
        res.send(JSON.stringify({ cards: results }));
        // Don't use the connection here, it has been returned to the pool.
        });
    });
});
app.get('/buttons', (req, res) => {
    pool.getConnection(function(err, connection) {
        if (err) throw err; // not connected!
    
        // Use the connection
        connection.query('SELECT * FROM buttons;', function (error, results, fields) {
        // When done with the connection, release it.
        connection.release();
    
        // Handle error after the release.
        if (error) throw error;
    
        res.send(JSON.stringify({ buttons: results }));
        // Don't use the connection here, it has been returned to the pool.
        });
    });
});
app.get('/lists', (req, res) => {
    pool.getConnection(function(err, connection) {
        if (err) throw err; // not connected!
    
        // Use the connection
        connection.query('SELECT * FROM lists;', function (error, results, fields) {
        // When done with the connection, release it.
        connection.release();
    
        // Handle error after the release.
        if (error) throw error;
    
        res.send(JSON.stringify({ lists: results }));
        // Don't use the connection here, it has been returned to the pool.
        });
    });
});
app.get('/accordions', (req, res) => {
    pool.getConnection(function(err, connection) {
        if (err) throw err; // not connected!
    
        // Use the connection
        connection.query('SELECT * FROM accordions;', function (error, results, fields) {
        // When done with the connection, release it.
        connection.release();
    
        // Handle error after the release.
        if (error) throw error;
    
        res.send(JSON.stringify({ accordions: results }));
        // Don't use the connection here, it has been returned to the pool.
        });
    });
});
app.get('/toolbarMenus', (req, res) => {
    pool.getConnection(function(err, connection) {
        if (err) throw err; // not connected!
    
        // Use the connection
        connection.query('SELECT * FROM toolbarMenu;', function (error, results, fields) {
        // When done with the connection, release it.
        connection.release();
    
        // Handle error after the release.
        if (error) throw error;
    
        res.send(JSON.stringify({ toolbarMenus: results }));
        // Don't use the connection here, it has been returned to the pool.
        });
    });
});
app.get('/slideshows', (req, res) => {
    pool.getConnection(function(err, connection) {
        if (err) throw err; // not connected!
    
        // Use the connection
        connection.query('SELECT * FROM slideshow;', function (error, results, fields) {
        // When done with the connection, release it.
        connection.release();
    
        // Handle error after the release.
        if (error) throw error;
    
        res.send(JSON.stringify({ slideshows: results }));
        // Don't use the connection here, it has been returned to the pool.
        });
    });
});
app.get('/shoppingCart', (req, res) => {
    pool.getConnection(function(err, connection) {
        if (err) throw err; // not connected!
    
        // Use the connection
        connection.query('SELECT * FROM shoppingCart;', function (error, results, fields) {
        // When done with the connection, release it.
        connection.release();
    
        // Handle error after the release.
        if (error) throw error;
    
        res.send(JSON.stringify({ shoppingCart: results }));
        // Don't use the connection here, it has been returned to the pool.
        });
    });
});
app.post('/shoppingCart', (req, res) => {
    var product = req.body;
    
    pool.getConnection(function(err, connection) {
        if (err) throw err; // not connected!
    
        // Use the connection
        connection.query('INSERT INTO shoppingCart (project,description,rate,quantity,price,priceWithVat,vat,vatNumerological) VALUES ("'+product.project+'","'+product.description+'","'+product.rate+'","'+product.quantity+'","'+product.price+'","'+product.priceWithVat+'","'+product.vat+'","'+product.vatNumerological+'");', function (error, results, fields) {
        // When done with the connection, release it.
        connection.release();
    
        // Handle error after the release.
        if (error) throw error;
    
        res.send(JSON.stringify({ shoppingCart: results }));
        // Don't use the connection here, it has been returned to the pool.
        });
    });
});

app.get('/products', (req, res) => {
    pool.getConnection(function(err, connection) {
        if (err) throw err; // not connected!
    
        // Use the connection
        connection.query('SELECT * FROM products;', function (error, results, fields) {
        // When done with the connection, release it.
        connection.release();
    
        // Handle error after the release.
        if (error) throw error;
    
        res.send(JSON.stringify({ products: results }));
        // Don't use the connection here, it has been returned to the pool.
        });
    });
});

app.get('/capacity', (req, res) => {
    pool.getConnection(function(err, connection) {
        if (err) throw err; // not connected!
    
        // Use the connection
        connection.query('SELECT * FROM productsCapacity;', function (error, results, fields) {
        // When done with the connection, release it.
        connection.release();
    
        // Handle error after the release.
        if (error) throw error;
    
        res.send(JSON.stringify({ capacity: results }));
        // Don't use the connection here, it has been returned to the pool.
        });
    });
});

app.get('/prices', (req, res) => {
    pool.getConnection(function(err, connection) {
        if (err) throw err; // not connected!
    
        // Use the connection
        connection.query('SELECT * FROM productsPrices;', function (error, results, fields) {
        // When done with the connection, release it.
        connection.release();
    
        // Handle error after the release.
        if (error) throw error;
    
        res.send(JSON.stringify({ prices: results }));
        // Don't use the connection here, it has been returned to the pool.
        });
    });
});
app.get('/kohde', (req, res) => {
    pool.getConnection(function(err, connection) {
        if (err) throw err; // not connected!
    
        // Use the connection
        connection.query('SELECT * FROM productsKohde;', function (error, results, fields) {
        // When done with the connection, release it.
        connection.release();
    
        // Handle error after the release.
        if (error) throw error;
    
        res.send(JSON.stringify({ kohde: results }));
        // Don't use the connection here, it has been returned to the pool.
        });
    });
});
app.listen(PORT, () => {
    console.log('Listening on port', PORT, '...');
});