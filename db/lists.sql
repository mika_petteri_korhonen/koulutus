CREATE TABLE `lists` (
  `idlists` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `listOrder` int(11) DEFAULT NULL,
  `subHeader` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `additionalLine` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `header` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`idlists`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
