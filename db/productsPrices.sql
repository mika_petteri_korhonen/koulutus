CREATE TABLE `productsPrices` (
  `idproductPrices` int(11) NOT NULL AUTO_INCREMENT,
  `productId` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `date` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`idproductPrices`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
