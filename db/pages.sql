CREATE TABLE `pages` (
  `idpage` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `content` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `class` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `params` longtext COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`idpage`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
