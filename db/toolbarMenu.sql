CREATE TABLE `toolbarMenu` (
  `idtoolbar` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `level` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `class` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`idtoolbar`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
