CREATE TABLE `forms` (
  `idforms` int(11) NOT NULL AUTO_INCREMENT,
  `elementsIds` longtext COLLATE utf8_bin DEFAULT NULL,
  `submitCallFunction` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`idforms`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
