CREATE TABLE `elements` (
  `idelements` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `elementOrder` int(11) DEFAULT NULL,
  `classes` longtext COLLATE utf8_bin DEFAULT NULL,
  `pageId` int(11) DEFAULT NULL,
  PRIMARY KEY (`idelements`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
