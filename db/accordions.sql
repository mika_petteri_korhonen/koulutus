CREATE TABLE `accordions` (
  `idaccordions` int(11) NOT NULL AUTO_INCREMENT,
  `header` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `subHeader` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `contentElems` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`idaccordions`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
