CREATE TABLE `tabs` (
  `idtabs` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `tabsOrder` int(11) DEFAULT NULL,
  `elementsIds` longtext COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`idtabs`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
