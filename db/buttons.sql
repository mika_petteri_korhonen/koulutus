CREATE TABLE `buttons` (
  `idbuttons` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `clickFunction` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`idbuttons`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
