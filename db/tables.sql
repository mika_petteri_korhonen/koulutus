CREATE TABLE `tables` (
  `idtables` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `value` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `tableId` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `colName` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`idtables`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
