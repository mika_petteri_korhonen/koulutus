CREATE TABLE `cards` (
  `idcards` int(11) NOT NULL AUTO_INCREMENT,
  `header` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `subHeader` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `buttons` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `contentElems` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`idcards`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
