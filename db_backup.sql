-- MySQL dump 10.16  Distrib 10.2.19-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: education
-- ------------------------------------------------------
-- Server version	10.2.19-MariaDB-10.2.19+maria~xenial

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accordions`
--

DROP TABLE IF EXISTS `accordions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accordions` (
  `idaccordions` int(11) NOT NULL AUTO_INCREMENT,
  `header` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `subHeader` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `contentElems` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`idaccordions`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accordions`
--

LOCK TABLES `accordions` WRITE;
/*!40000 ALTER TABLE `accordions` DISABLE KEYS */;
INSERT INTO `accordions` VALUES (1,'Haitari 1','Sub Header','2,'),(2,'Haitari 2','Sub Header','4,');
/*!40000 ALTER TABLE `accordions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `buttons`
--

DROP TABLE IF EXISTS `buttons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `buttons` (
  `idbuttons` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `clickFunction` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `style` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `productCode` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`idbuttons`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `buttons`
--

LOCK TABLES `buttons` WRITE;
/*!40000 ALTER TABLE `buttons` DISABLE KEYS */;
INSERT INTO `buttons` VALUES (1,'Test','test','primary','mat-raised-button',NULL),(2,'Test2','test2','accent','mat-raised-button',NULL),(3,'Warn','warn','warn','mat-flat-button',NULL),(4,'Test','test','primary','mat-button',NULL),(5,'Test','test','accent','mat-fab',NULL),(6,'test','test','warn','mat-mini-fab',NULL),(7,'Lisää tuote','addProduct','primary','mat-raised-button','0'),(8,'Lisää tuote','addProduct','primary','mat-raised-button','1'),(9,'Lisää tuote','addProduct','primary','mat-raised-button','2');
/*!40000 ALTER TABLE `buttons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cards`
--

DROP TABLE IF EXISTS `cards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cards` (
  `idcards` int(11) NOT NULL AUTO_INCREMENT,
  `header` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `subHeader` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `buttons` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `contentElems` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `productId` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`idcards`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cards`
--

LOCK TABLES `cards` WRITE;
/*!40000 ALTER TABLE `cards` DISABLE KEYS */;
INSERT INTO `cards` VALUES (1,'Hiihtoloma laskettelu','Hiihtoloma','8','1,','/assets/free.jpeg','1'),(2,'Syysloma Rukalla','Syysloma','7','1,','/assets/free1.jpeg','2'),(3,'Joululoma Hiitäen','Joululoma','9','1,','/assets/free2.jpeg','3');
/*!40000 ALTER TABLE `cards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elements`
--

DROP TABLE IF EXISTS `elements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elements` (
  `idelements` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `elementOrder` int(11) DEFAULT NULL,
  `classes` longtext COLLATE utf8_bin DEFAULT NULL,
  `pageId` int(11) DEFAULT NULL,
  PRIMARY KEY (`idelements`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elements`
--

LOCK TABLES `elements` WRITE;
/*!40000 ALTER TABLE `elements` DISABLE KEYS */;
INSERT INTO `elements` VALUES (1,'tab',1,'full-width',1),(2,'table',1,'full-width',1),(3,'card',1,'full-width',1),(4,'list',1,'full-width',1),(5,'accordion',1,'full-width',1),(6,'toolbarMenu',1,'full-width',1),(7,'card',2,'full-width',1),(8,'card',3,'full-width',1);
/*!40000 ALTER TABLE `elements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forms`
--

DROP TABLE IF EXISTS `forms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forms` (
  `idforms` int(11) NOT NULL AUTO_INCREMENT,
  `elementsIds` longtext COLLATE utf8_bin DEFAULT NULL,
  `submitCallFunction` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`idforms`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forms`
--

LOCK TABLES `forms` WRITE;
/*!40000 ALTER TABLE `forms` DISABLE KEYS */;
/*!40000 ALTER TABLE `forms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lists`
--

DROP TABLE IF EXISTS `lists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lists` (
  `idlists` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `listOrder` int(11) DEFAULT NULL,
  `subHeader` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `additionalLine` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `header` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`idlists`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lists`
--

LOCK TABLES `lists` WRITE;
/*!40000 ALTER TABLE `lists` DISABLE KEYS */;
INSERT INTO `lists` VALUES (1,'Test',1,'sub header','folder','Additional line','Header'),(2,'Test 2',2,'sub header','folder','Additional line','Header'),(4,'Test 3',3,'sub header','folder','Additional line','Header');
/*!40000 ALTER TABLE `lists` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `idpage` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `content` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `class` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `params` longtext COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`idpage`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `idproducts` int(11) NOT NULL AUTO_INCREMENT,
  `kohdeCode` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `productCode` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `project` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `rate` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `quantity` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `price` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `priceWithVat` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `vat` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `vatNumerological` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`idproducts`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,'ALPMOK1','MAJ','Alppihimos1 Majoitustuote','Luksusluokan mökki porealtaalla ja saunalla.','200','1','200','248','24%','0.24'),(2,'ALPMOK1','LII','Alppihimos1 Liinavaatesetti','Lisäpalvelu','200','1','200','248','24%','0.24'),(3,'ALPMOK1','SII','Alppihimos1 Loppusiivous','Lisäpalvelu','20','1','20','24.8','24%','0.24'),(4,'ALPMOK2','MAJ','Alppihimos2 Majoitustuote','HimosYkkösen mökkialue sijaitsee Länsi-Himoksen rinteiden läheisyyessä. Alue on kävelymatkan päässä Himos Centerin, ravintoloiden ja rinteiden palveuista, ja alueelta on kauniit näkymät Kuikkajärvelle ja Länsi-Himoksen rinteisiin.',NULL,NULL,NULL,NULL,NULL,NULL),(5,'ALPMOK2','LII','Alppihimos2 Liinavaatesetti','Lisäpalvelu',NULL,NULL,NULL,NULL,NULL,NULL),(6,'ALPMOK2','SII','Alppihimos2 Loppusiivous','Lisäpalvelu',NULL,NULL,NULL,NULL,NULL,NULL),(7,'HYKKO1','MAJ','HimosYkkönen1 Majoitustuote','HimosYkkösen mökkialue sijaitsee Länsi-Himoksen rinteiden läheisyyessä. Alue on kävelymatkan päässä Himos Centerin, ravintoloiden ja rinteiden palveuista, ja alueelta on kauniit näkymät Kuikkajärvelle ja Länsi-Himoksen rinteisiin.',NULL,NULL,NULL,NULL,NULL,NULL),(8,'HYKKO1','LII','HimosYkkönen1 Liinavaatesetti','Lisäpalvelu',NULL,NULL,NULL,NULL,NULL,NULL),(9,'HYKKO1','SII','HimosYkkönen1 Loppusiivous','Lisäpalvelu',NULL,NULL,NULL,NULL,NULL,NULL),(10,'HYKKO2','MAJ','HimosYkkönen2 Majoitustuote','HimosYkkösen mökkialue sijaitsee Länsi-Himoksen rinteiden läheisyyessä. Alue on kävelymatkan päässä Himos Centerin, ravintoloiden ja rinteiden palveuista, ja alueelta on kauniit näkymät Kuikkajärvelle ja Länsi-Himoksen rinteisiin.',NULL,NULL,NULL,NULL,NULL,NULL),(11,'HYKKO2','LII','HimosYkkönen2 Liinavaatesetti','Lisäpalvelu',NULL,NULL,NULL,NULL,NULL,NULL),(12,'HYKKO2','SII','HimosYkkönen2 Loppusiivous','Lisäpalvelu',NULL,NULL,NULL,NULL,NULL,NULL),(13,'MSAFAR1','MAJ','Mönkiäsafari1 Majoitustuote','Ajetaan Yläiselle Liesjärvelle, Himoksen rinteiden taakse, jossa laavulla paistetaan makkarat ja nautitaan mehut. Ajellaan pienempiä kyläteitä ja kurvataan metsäpolkuja pitkin. ',NULL,NULL,NULL,NULL,NULL,NULL),(14,'MSAFAR1','LII','Mönkiäsafari1 Liinavaatesetti','Lisäpalvelu',NULL,NULL,NULL,NULL,NULL,NULL),(15,'MSAFAR1','SII','Mönkiäsafari1 Loppusiivous','Lisäpalvelu',NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productsCapacity`
--

DROP TABLE IF EXISTS `productsCapacity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productsCapacity` (
  `idproductsCapacity` int(11) NOT NULL AUTO_INCREMENT,
  `kohdeCode` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `productCode` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `capacity` int(11) DEFAULT NULL,
  `usedCapacity` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `date` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`idproductsCapacity`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productsCapacity`
--

LOCK TABLES `productsCapacity` WRITE;
/*!40000 ALTER TABLE `productsCapacity` DISABLE KEYS */;
INSERT INTO `productsCapacity` VALUES (1,'ALPMOK1','MAJ',1,'0','1.1.2019'),(2,'ALPMOK1','MAJ',1,'0','2.1.2019'),(3,'ALPMOK1','MAJ',1,'0','3.1.2019'),(4,'ALPMOK1','MAJ',1,'0','4.1.2019'),(5,'ALPMOK1','MAJ',1,'0','5.1.2019'),(6,'ALPMOK1','MAJ',1,'0','6.1.2019'),(7,'ALPMOK1','MAJ',1,'0','7.1.2019'),(8,'ALPMOK1','MAJ',1,'0','8.1.2019'),(9,'ALPMOK1','MAJ',1,'0','9.1.2019'),(10,'ALPMOK1','MAJ',1,'0','10.1.2019'),(11,'ALPMOK2','MAJ',1,'0','1.1.2019'),(12,'ALPMOK2','MAJ',1,'0','2.1.2019'),(13,'ALPMOK2','MAJ',1,'0','3.1.2019'),(14,'ALPMOK2','MAJ',1,'0','4.1.2019'),(15,'ALPMOK2','MAJ',1,'0','5.1.2019'),(16,'ALPMOK2','MAJ',1,'0','6.1.2019'),(17,'ALPMOK2','MAJ',1,'0','7.1.2019'),(18,'ALPMOK2','MAJ',1,'0','8.1.2019'),(19,'ALPMOK2','MAJ',1,'0','9.1.2019'),(20,'ALPMOK2','MAJ',1,'0','10.1.2019'),(21,'HYKKO1','MAJ',1,'0','1.1.2019'),(22,'HYKKO1','MAJ',1,'0','2.1.2019'),(23,'HYKKO1','MAJ',1,'0','3.1.2019'),(24,'HYKKO1','MAJ',1,'0','4.1.2019'),(25,'HYKKO1','MAJ',1,'0','5.1.2019'),(26,'HYKKO1','MAJ',1,'0','6.1.2019'),(27,'HYKKO1','MAJ',1,'0','7.1.2019'),(28,'HYKKO1','MAJ',1,'0','8.1.2019'),(29,'HYKKO1','MAJ',1,'0','9.1.2019'),(30,'HYKKO1','MAJ',1,'0','10.1.2019'),(31,'HYKKO2','MAJ',1,'0','1.1.2019'),(32,'HYKKO2','MAJ',1,'0','2.1.2019'),(33,'HYKKO2','MAJ',1,'0','3.1.2019'),(34,'HYKKO2','MAJ',1,'0','4.1.2019'),(35,'HYKKO2','MAJ',1,'0','5.1.2019'),(36,'HYKKO2','MAJ',1,'0','6.1.2019'),(37,'HYKKO2','MAJ',1,'0','7.1.2019'),(38,'HYKKO2','MAJ',1,'0','8.1.2019'),(39,'HYKKO2','MAJ',1,'0','9.1.2019'),(40,'HYKKO2','MAJ',1,'0','10.1.2019'),(41,'MSAFAR1','MAJ',10,'0','1.1.2019'),(42,'MSAFAR1','MAJ',10,'0','2.1.2019'),(43,'MSAFAR1','MAJ',10,'0','3.1.2019'),(44,'MSAFAR1','MAJ',10,'0','4.1.2019'),(45,'MSAFAR1','MAJ',10,'0','5.1.2019'),(46,'MSAFAR1','MAJ',10,'0','6.1.2019'),(47,'MSAFAR1','MAJ',10,'0','7.1.2019'),(48,'MSAFAR1','MAJ',10,'0','8.1.2019'),(49,'MSAFAR1','MAJ',10,'0','9.1.2019'),(50,'MSAFAR1','MAJ',10,'0','10.1.2019');
/*!40000 ALTER TABLE `productsCapacity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productsKohde`
--

DROP TABLE IF EXISTS `productsKohde`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productsKohde` (
  `idproductsKohde` int(11) NOT NULL AUTO_INCREMENT,
  `kohdeTyyppi` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `kohdeCode` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`idproductsKohde`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productsKohde`
--

LOCK TABLES `productsKohde` WRITE;
/*!40000 ALTER TABLE `productsKohde` DISABLE KEYS */;
INSERT INTO `productsKohde` VALUES (1,'MAJ','ALPMOK1','Alppihimos1','Luksusluokan mökki porealtaalla ja saunalla.','/assets/free.jpeg'),(2,'MAJ','ALPMOK2','Alppihimos2','HimosYkkösen mökkialue sijaitsee Länsi-Himoksen rinteiden läheisyyessä. Alue on kävelymatkan päässä Himos Centerin, ravintoloiden ja rinteiden palveuista, ja alueelta on kauniit näkymät Kuikkajärvelle ja Länsi-Himoksen rinteisiin.','/assets/free1.jpeg'),(3,'MAJ','HYKKO1','HimosYkkönen1','HimosYkkösen mökkialue sijaitsee Länsi-Himoksen rinteiden läheisyyessä. Alue on kävelymatkan päässä Himos Centerin, ravintoloiden ja rinteiden palveuista, ja alueelta on kauniit näkymät Kuikkajärvelle ja Länsi-Himoksen rinteisiin.','/assets/free2.jpeg'),(4,'MAJ','HYKKO2','HimosYkkönen2','HimosYkkösen mökkialue sijaitsee Länsi-Himoksen rinteiden läheisyyessä. Alue on kävelymatkan päässä Himos Centerin, ravintoloiden ja rinteiden palveuista, ja alueelta on kauniit näkymät Kuikkajärvelle ja Länsi-Himoksen rinteisiin.','/assets/free3.jpeg'),(5,'AKT','MSAFAR1','Mönkiäsafari1','Ajetaan Yläiselle Liesjärvelle, Himoksen rinteiden taakse, jossa laavulla paistetaan makkarat ja nautitaan mehut. Ajellaan pienempiä kyläteitä ja kurvataan metsäpolkuja pitkin. ','/assets/free4.jpeg');
/*!40000 ALTER TABLE `productsKohde` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productsPrices`
--

DROP TABLE IF EXISTS `productsPrices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productsPrices` (
  `idproductPrices` int(11) NOT NULL AUTO_INCREMENT,
  `kohdeCode` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `productCode` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `date` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`idproductPrices`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productsPrices`
--

LOCK TABLES `productsPrices` WRITE;
/*!40000 ALTER TABLE `productsPrices` DISABLE KEYS */;
INSERT INTO `productsPrices` VALUES (1,'ALPMOK1','MAJ',150,'1.1.2019'),(2,'ALPMOK1','MAJ',150,'2.1.2019'),(3,'ALPMOK1','MAJ',150,'3.1.2019'),(4,'ALPMOK1','MAJ',150,'4.1.2019'),(5,'ALPMOK1','MAJ',150,'5.1.2019'),(6,'ALPMOK1','MAJ',150,'6.1.2019'),(7,'ALPMOK1','MAJ',150,'7.1.2019'),(8,'ALPMOK1','MAJ',150,'8.1.2019'),(9,'ALPMOK1','MAJ',150,'9.1.2019'),(10,'ALPMOK1','MAJ',150,'10.1.2019'),(11,'ALPMOK2','MAJ',150,'2.1.2019'),(12,'ALPMOK2','MAJ',150,'3.1.2019'),(13,'ALPMOK2','MAJ',150,'4.1.2019'),(14,'ALPMOK2','MAJ',150,'5.1.2019'),(15,'ALPMOK2','MAJ',150,'6.1.2019'),(16,'ALPMOK2','MAJ',150,'7.1.2019'),(17,'ALPMOK2','MAJ',150,'8.1.2019'),(18,'ALPMOK2','MAJ',150,'9.1.2019'),(19,'ALPMOK2','MAJ',150,'10.1.2019'),(20,'HYKKO1','MAJ',150,'2.1.2019'),(21,'HYKKO1','MAJ',150,'3.1.2019'),(22,'HYKKO1','MAJ',150,'4.1.2019'),(23,'HYKKO1','MAJ',150,'5.1.2019'),(24,'HYKKO1','MAJ',150,'6.1.2019'),(25,'HYKKO1','MAJ',150,'7.1.2019'),(26,'HYKKO1','MAJ',150,'8.1.2019'),(27,'HYKKO1','MAJ',150,'9.1.2019'),(28,'HYKKO1','MAJ',150,'10.1.2019'),(29,'HYKKO2','MAJ',150,'2.1.2019'),(30,'HYKKO2','MAJ',150,'3.1.2019'),(31,'HYKKO2','MAJ',150,'4.1.2019'),(32,'HYKKO2','MAJ',150,'5.1.2019'),(33,'HYKKO2','MAJ',150,'6.1.2019'),(34,'HYKKO2','MAJ',150,'7.1.2019'),(35,'HYKKO2','MAJ',150,'8.1.2019'),(36,'HYKKO2','MAJ',150,'9.1.2019'),(37,'HYKKO2','MAJ',150,'10.1.2019'),(38,'MSAFAR1','AKT',150,'2.1.2019'),(39,'MSAFAR1','AKT',150,'3.1.2019'),(40,'MSAFAR1','AKT',150,'4.1.2019'),(41,'MSAFAR1','AKT',150,'5.1.2019'),(42,'MSAFAR1','AKT',150,'6.1.2019'),(43,'MSAFAR1','AKT',150,'7.1.2019'),(44,'MSAFAR1','AKT',150,'8.1.2019'),(45,'MSAFAR1','AKT',150,'9.1.2019'),(46,'MSAFAR1','AKT',150,'10.1.2019');
/*!40000 ALTER TABLE `productsPrices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shoppingCart`
--

DROP TABLE IF EXISTS `shoppingCart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shoppingCart` (
  `idshoppingCart` int(11) NOT NULL AUTO_INCREMENT,
  `project` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `rate` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `quantity` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `price` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `priceWithVat` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `vat` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `vatNumerological` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`idshoppingCart`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shoppingCart`
--

LOCK TABLES `shoppingCart` WRITE;
/*!40000 ALTER TABLE `shoppingCart` DISABLE KEYS */;
INSERT INTO `shoppingCart` VALUES (12,'Hiihtoloma laskettelu','Laskettelu hiihtolomalla rukan tunturissa.','200','1','200','248','24%','0.24'),(13,'Joululoma Hiitäen','Hiihdä kivikon laduilla','20','1','20','24.8','24%','0.24'),(14,'Syysloma Rukalla','Laskettelu hiihtolomalla rukan tunturissa.','200','1','200','248','24%','0.24'),(15,'Hiihtoloma laskettelu','Laskettelu hiihtolomalla rukan tunturissa.','200','1','200','248','24%','0.24'),(16,'Joululoma Hiitäen','Hiihdä kivikon laduilla','20','1','20','24.8','24%','0.24');
/*!40000 ALTER TABLE `shoppingCart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `slideshow`
--

DROP TABLE IF EXISTS `slideshow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `slideshow` (
  `idslideshow` int(11) NOT NULL AUTO_INCREMENT,
  `images` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `width` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `height` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `buttons` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `arrows` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`idslideshow`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `slideshow`
--

LOCK TABLES `slideshow` WRITE;
/*!40000 ALTER TABLE `slideshow` DISABLE KEYS */;
INSERT INTO `slideshow` VALUES (1,'/assets/free.jpeg,/assets/free1.jpeg,/assets/free2.jpeg,/assets/free3.jpeg,/assets/free4.jpeg','90%','750px','true','true');
/*!40000 ALTER TABLE `slideshow` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `soldProducts`
--

DROP TABLE IF EXISTS `soldProducts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `soldProducts` (
  `idsoldProducts` int(11) NOT NULL AUTO_INCREMENT,
  `productId` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `referenceId` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `invoiceLocation` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `userId` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`idsoldProducts`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `soldProducts`
--

LOCK TABLES `soldProducts` WRITE;
/*!40000 ALTER TABLE `soldProducts` DISABLE KEYS */;
/*!40000 ALTER TABLE `soldProducts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tables`
--

DROP TABLE IF EXISTS `tables`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tables` (
  `idtables` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `value` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `tableId` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `colName` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`idtables`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tables`
--

LOCK TABLES `tables` WRITE;
/*!40000 ALTER TABLE `tables` DISABLE KEYS */;
INSERT INTO `tables` VALUES (1,'string','1','1','position'),(2,'string','Hydrogen','1','name'),(3,'string','1.0079','1','weight'),(4,'string','H','1','symbol'),(5,'string','2','1','position'),(6,'string','Helium','1','name'),(7,'string','4.0026','1','weight'),(8,'string','He','1','symbol'),(9,'string','3','1','position'),(10,'string','Lithium','1','name'),(11,'string','6.941','1','weight'),(12,'string','Li','1','symbol');
/*!40000 ALTER TABLE `tables` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabs`
--

DROP TABLE IF EXISTS `tabs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabs` (
  `idtabs` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `tabsOrder` int(11) DEFAULT NULL,
  `elementsIds` longtext COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`idtabs`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabs`
--

LOCK TABLES `tabs` WRITE;
/*!40000 ALTER TABLE `tabs` DISABLE KEYS */;
INSERT INTO `tabs` VALUES (1,1,'Test',1,'2,'),(2,1,'Test2',2,'4,'),(3,1,'Haitari',3,'5,');
/*!40000 ALTER TABLE `tabs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `toolbarMenu`
--

DROP TABLE IF EXISTS `toolbarMenu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `toolbarMenu` (
  `idtoolbar` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `level` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `class` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`idtoolbar`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `toolbarMenu`
--

LOCK TABLES `toolbarMenu` WRITE;
/*!40000 ALTER TABLE `toolbarMenu` DISABLE KEYS */;
INSERT INTO `toolbarMenu` VALUES (1,'2,3,4,5,6,7,','Test Menu',1,'1','first',NULL),(2,'','Menu item',1,'2','second','/menu'),(3,'','Menu item 2',2,'2','third','/menu-1'),(4,'','Sub menu item',1,'2','forth','/menu-2'),(5,'','Sub menu item 2',2,'2','fifth','/menu-3'),(6,'','Sub menu item 3',1,'2','sixth','/menu-4'),(7,'','Sub menu item 4',2,'2','seventh','/menu-5');
/*!40000 ALTER TABLE `toolbarMenu` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-20 15:35:20
